package com.channat.models.dto;

public class Student extends Base {

    private String firstName;
    private String lastName;
    private String gender;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
//        if (firstName.isEmpty()) {
//            this.firstName = null;
//        } else {
//            this.firstName = firstName;
//        }
        this.firstName = firstName.trim().isEmpty() ? null : firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.trim().isEmpty() ? null : lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender.trim().isEmpty() ? null : gender.toUpperCase().charAt(0) + "";
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
