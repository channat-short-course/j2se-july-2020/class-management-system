package com.channat.models.dao;

import com.channat.models.dto.Course;
import com.channat.models.dto.Student;
import com.channat.models.dto.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CourseDAO {


    CMSConnection cmsConnection = new CMSConnection();
    private PreparedStatement preparedStatement;

    public boolean add(Course course) {
        boolean isSucceed = false;
        String sql = "INSERT INTO cm_courses(course_name, teacher_id) VALUES (?, ?);";
        cmsConnection.openConnection();

        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, course.getCourseName());
            preparedStatement.setInt(2, course.getTeacher().getId());
            int numberOfTeacherInserted = preparedStatement.executeUpdate();
            if (numberOfTeacherInserted > 0 ) isSucceed = true;

            if (course.getStudents().size()>0) {
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    int courseId = resultSet.getInt(1);
                    String studentCourseSQL = "INSERT INTO cm_student_course (course_id, student_id) VALUES ";
                    for (int i = 0; i < course.getStudents().size(); i++) {
                        studentCourseSQL += "(" + courseId + ", " + course.getStudents().get(i).getId() + "),";
                    }
                    // we need to remove "," at the end of the String
                    studentCourseSQL = studentCourseSQL.substring(0, studentCourseSQL.length() - 1);
                    Statement statement = cmsConnection.getConnection().createStatement();
                    statement.executeUpdate(studentCourseSQL);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }



        return isSucceed;
    }

    public ArrayList<Course> getAll() {
        ArrayList<Course> courses = new ArrayList<>();
        String sql = "SELECT c.id, c.course_name, ct.first_name, ct.last_name\n" +
                "FROM cm_courses c\n" +
                "JOIN cm_teachers ct on c.teacher_id = ct.id\n" +
                "WHERE c.status IS TRUE;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            Course course;
            Teacher teacher;
            while (resultSet.next()) {
                course = new Course();
                teacher = new Teacher();
                course.setId(resultSet.getInt(1));
                course.setCourseName(resultSet.getString(2));

                teacher.setFirstName(resultSet.getString(3));
                teacher.setLastName(resultSet.getString(4));

                course.setTeacher(teacher);
                courses.add(course);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }
        return courses;
    }

    public Course detail(int courseId, Course course) {
        String sql = "SELECT s.first_name, s.last_name\n" +
                "FROM cm_student_course sc\n" +
                "JOIN cm_students s on sc.student_id = s.id\n" +
                "WHERE course_id = ?;\n";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Student student;
            ArrayList<Student> students = new ArrayList<>();
            while (resultSet.next()) {
                student = new Student();
                student.setFirstName(resultSet.getString(1));
                student.setLastName(resultSet.getString(2));
                students.add(student);
            }
            course.setStudents(students);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return course;
    }

    public boolean delete(int courseId) {
        boolean isSucceed = false;
        String sql = "UPDATE cm_courses SET status = FALSE\n" +
                "WHERE id = ?;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, courseId);
            int numberOfRowAffected = preparedStatement.executeUpdate();
            if (numberOfRowAffected > 0)
                isSucceed = true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return isSucceed;
    }

    public boolean edit(int courseId, Course course) {
        boolean isSucceed = false;
        String sql = "UPDATE cm_courses SET course_name = ? , teacher_id = ? \n" +
                "WHERE id = ? ;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, course.getCourseName());
            preparedStatement.setInt(2, course.getTeacher().getId());
            preparedStatement.setInt(3, courseId);
            int result = preparedStatement.executeUpdate();
            if (result > 0) isSucceed = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }

        return isSucceed;
    }

}
