package com.channat.models.dao;

import com.channat.models.dto.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TeacherDAO {

    CMSConnection cmsConnection = new CMSConnection();
    private PreparedStatement preparedStatement;

    // get all teacher
    public ArrayList<Teacher> getAll() {
        ArrayList<Teacher> teachers = new ArrayList<>();
        String sql = "SELECT * FROM cm_teachers\n" +
                "WHERE status IS TRUE;";

        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            Teacher teacher;
            while (resultSet.next()) {
                teacher = new Teacher();
                teacher.setId(resultSet.getInt(1));
                teacher.setFirstName(resultSet.getString(2));
                teacher.setLastName(resultSet.getString(3));
                teacher.setGender(resultSet.getString(4));
                teacher.setSpecialist(resultSet.getString(5));
                teacher.setSalary(resultSet.getFloat(6));
                teacher.setStatus(resultSet.getBoolean(7));
                teacher.setCreatedDate(resultSet.getString(8));

                teachers.add(teacher);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }


        return teachers;
    }

    public boolean add(Teacher teacher) {
        boolean addedSuccess = false;
        String sql = "INSERT INTO cm_teachers (first_name, last_name, gender, specialist, salary)  \n" +
                "VALUES (?, ?, ?, ?, ?)";
        cmsConnection.openConnection();

        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, teacher.getFirstName());
            preparedStatement.setString(2, teacher.getLastName());
            preparedStatement.setString(3, teacher.getGender());
            preparedStatement.setString(4, teacher.getSpecialist());
            preparedStatement.setFloat(5, teacher.getSalary());
            int numberOfInsertedRow = preparedStatement.executeUpdate();
            if (numberOfInsertedRow > 0)
                addedSuccess = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }
        return addedSuccess;
    }

    public Teacher getDetail(int id) {
        Teacher teacher = new Teacher();
        String sql = "SELECT * FROM cm_teachers\n" +
                "WHERE id = ? AND status is TRUE";
        cmsConnection.openConnection();

        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                teacher.setId(resultSet.getInt("id"));
                teacher.setFirstName(resultSet.getString("first_name"));
                teacher.setLastName(resultSet.getString("last_name"));
                teacher.setGender(resultSet.getString("gender"));
                teacher.setSpecialist(resultSet.getString("specialist"));
                teacher.setSalary(resultSet.getFloat("salary"));
                teacher.setStatus(resultSet.getBoolean("status"));
                teacher.setCreatedDate(resultSet.getString("created_date"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }

        return teacher;
    }

    public boolean update(int id, Teacher teacher) {
        boolean isSucceed = false;
        String sql = "UPDATE cm_teachers " +
                "SET first_name = ?, last_name = ?, gender = ?, specialist = ?, salary = ? " +
                "WHERE id = ?;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, teacher.getFirstName());
            preparedStatement.setString(2, teacher.getLastName());
            preparedStatement.setString(3, teacher.getGender());
            preparedStatement.setString(4, teacher.getSpecialist());
            preparedStatement.setFloat(5, teacher.getSalary());
            preparedStatement.setInt(6, id);
            int numberOfRowUpdated = preparedStatement.executeUpdate();
            if (numberOfRowUpdated > 0)
                isSucceed = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }
        return isSucceed;
    }

    public boolean delete(int id) {
        boolean isSucceed = false;
        String sql = "UPDATE cm_teachers SET status = false WHERE id = ?";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int numberOfDelete = preparedStatement.executeUpdate();
            if (numberOfDelete > 0)
                isSucceed = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }
        return isSucceed;
    }

}
