package com.channat.models.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CMSConnection {

    private Connection connection = null;

    public void openConnection() {
        String url = "jdbc:postgresql://localhost:5432/cms_db_test";
        String username = "cms_user_test";
        String password = "cms_password_test";
        try {
            connection = DriverManager.getConnection(url, username, password);
//            System.out.println("connected....");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }




}
