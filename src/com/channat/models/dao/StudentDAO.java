package com.channat.models.dao;

import com.channat.models.dto.Student;

import java.lang.reflect.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

 /*
    1. Add Student
    2. Edit Student
    3. Delete Student (Change status to false)
    4. Retrieve Detail
    5. Retrieve List
  */


public class StudentDAO {

    CMSConnection cmsConnection = new CMSConnection();
    private PreparedStatement preparedStatement;

    public Boolean add(Student student) {
        boolean isInserted = false;
        cmsConnection.openConnection();
        String sql = "INSERT INTO cm_students (first_name, last_name, gender) " +
                "VALUES (?, ?, ?);";
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setString(3, student.getGender());
            int numberOfRowInserted = preparedStatement.executeUpdate();
            if (numberOfRowInserted > 0)
                isInserted = true;
        } catch (SQLException throwables) {
//            throwables.printStackTrace();
        } finally {
            cmsConnection.closeConnection();
        }
        return isInserted;
    }

    public Boolean update(Student student) {
        boolean isUpdated = false;
        String sql = "UPDATE cm_students SET first_name = ?, last_name = ?, gender = ? \n" +
                "WHERE id=?;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setString(3, student.getGender());
            preparedStatement.setInt(4, student.getId());
            int numberOfRowUpdated =  preparedStatement.executeUpdate();
            if (numberOfRowUpdated > 0)
                isUpdated = true;
        } catch (SQLException e) {

        } finally {
            cmsConnection.closeConnection();
        }
        return isUpdated;
    }

    public Boolean delete(int id) {
        boolean isDeleted = false;
        String sql = "UPDATE cm_students SET status = false " +
                "WHERE id = ?;";
        cmsConnection.openConnection();

        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int numberOfRowAffected = preparedStatement.executeUpdate();
            if (numberOfRowAffected > 0)
                isDeleted = true;

        } catch (SQLException e) {

        } finally {
            cmsConnection.closeConnection();
        }

        return isDeleted;
    }

    public Student getDetail(int id) {
        Student student = new Student();
        String sql = "SELECT * FROM cm_students WHERE id = ? AND status IS TRUE;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                student.setId(resultSet.getInt(1));
                student.setFirstName(resultSet.getString(2));
                student.setLastName( resultSet.getString(3) );
                student.setGender(resultSet.getString(4));
                student.setStatus(resultSet.getBoolean(5));
                student.setCreatedDate(resultSet.getString(6));
            }

        } catch (SQLException e) {

        } finally {
            cmsConnection.closeConnection();
        }
        return student;
    }

    public ArrayList<Student> getAll() {
        ArrayList<Student> students = new ArrayList<>();
        String sql = "SELECT * FROM cm_students " +
                "WHERE status IS TRUE " +
                "ORDER BY id;";
        cmsConnection.openConnection();
        try {
            preparedStatement = cmsConnection.getConnection().prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt(1));
                student.setFirstName(resultSet.getString(2));
                student.setLastName( resultSet.getString(3) );
                student.setGender(resultSet.getString(4));
                student.setStatus(resultSet.getBoolean(5));
                student.setCreatedDate(resultSet.getString(6));
                students.add(student);
            }
        } catch (SQLException e) {

        } finally {
            cmsConnection.closeConnection();
        }
        return students;
    }

}
