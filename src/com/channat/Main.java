package com.channat;

import com.channat.views.CourseView;
import com.channat.views.StudentView;
import com.channat.views.TeacherView;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main extends Thread  {

    Scanner scanner = new Scanner(System.in);
    private String message;

    public void mainMenu() {
        System.out.println("========= MAIN MENU ===============");
        System.out.println("1. Student \t 2. Teacher \t 3. Course \t 4. Exit");
        try {
            System.out.print("-> ");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    StudentView studentView = new StudentView();
                    studentView.menu();
                    break;
                case 2:
                    TeacherView teacherView = new TeacherView();
                    teacherView.menu();
                    break;
                case 3:
                    CourseView courseView = new CourseView();
                    courseView.menu();
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    mainMenu();
            }
        } catch (InputMismatchException e) {
            scanner.next();
            mainMenu();
        }
    }

    public Main(String message) {
        this.message = message;
    }

    public Main() {}

    @Override
    public void run() {
//        Welcome to Class Management....
        for (int i=0; i<message.length(); i++) {
            System.out.print(message.charAt(i));
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }

    public static void main(String[] args) throws InterruptedException {

        Main m1 = new Main("============= Welcome to Class Management ================");
        Main m2 = new Main("==================\n");
        Main m3 = new Main("*******************\n");

        m1.start();
        m1.join();

        m2.start();
        m3.start();

        m2.join();

        new Main().mainMenu();

    }
}
