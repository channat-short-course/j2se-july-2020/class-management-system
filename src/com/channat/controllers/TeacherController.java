package com.channat.controllers;

import com.channat.models.dao.TeacherDAO;
import com.channat.models.dto.Teacher;

import java.util.ArrayList;

public class TeacherController {

    TeacherDAO teacherDAO = new TeacherDAO();

    public ArrayList<Teacher> getAll() {
        return teacherDAO.getAll();
    }

    public boolean add(Teacher teacher) {
        return teacherDAO.add(teacher);
    }

    public Teacher getDetail(int id) {
        return teacherDAO.getDetail(id);
    }

    public boolean update(int id, Teacher teacher) {
        return teacherDAO.update(id, teacher);
    }

    public boolean delete(int id) {
        return teacherDAO.delete(id);
    }

}
