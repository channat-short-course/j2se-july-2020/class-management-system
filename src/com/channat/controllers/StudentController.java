package com.channat.controllers;

import com.channat.models.dao.StudentDAO;
import com.channat.models.dto.Student;

import java.util.ArrayList;

   /*
    1. Add Student
    2. Edit Student
    3. Delete Student (Change status to false)
    4. Retrieve Detail
    5. Retrieve List
  */

public class StudentController {

    StudentDAO studentDAO = new StudentDAO();

    public Boolean add(Student student) {
        boolean isInserted = false;
        isInserted = studentDAO.add(student);
        return isInserted;
    }

    public Boolean update(Student student) {
        boolean isUpdated = false;
        isUpdated = studentDAO.update(student);
        return isUpdated;
    }

    public Boolean delete(int id) {
        boolean isDeleted = false;
        isDeleted = studentDAO.delete(id);
        return isDeleted;
    }

    public Student getDetail(int id) {
        Student student = new Student();
        student = studentDAO.getDetail(id);
        return student;
    }

    public ArrayList<Student> getAll() {
        ArrayList<Student> students = new ArrayList<>();
        students = studentDAO.getAll();
        return students;
    }

}
