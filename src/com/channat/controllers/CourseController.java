package com.channat.controllers;

import com.channat.models.dao.CourseDAO;
import com.channat.models.dto.Course;

import java.util.ArrayList;

public class CourseController {

    CourseDAO courseDAO = new CourseDAO();

    public boolean add(Course course) {
        return courseDAO.add(course);
    }

    public ArrayList<Course> getAll() {
        return courseDAO.getAll();
    }

    public Course detail(int courseId, Course course) {
        return courseDAO.detail(courseId, course);
    }

    public boolean delete(int courseId) {
        return courseDAO.delete(courseId);
    }

    public boolean edit(int courseId, Course course) {
        return courseDAO.edit(courseId, course);
    }


}
