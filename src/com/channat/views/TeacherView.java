package com.channat.views;

import com.channat.Main;
import com.channat.controllers.TeacherController;
import com.channat.models.dto.Teacher;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TeacherView {

    Scanner scanner;
    Teacher teacher;
    TeacherController teacherController;

    public void getAll() {
        teacherController = new TeacherController();
        ArrayList<Teacher> teachers = teacherController.getAll();
        if (!teachers.isEmpty()) {
            for (Teacher teacher : teachers) {
                System.out.println("------------");
                System.out.println("Id: " + teacher.getId());
                System.out.println("First name: " + teacher.getFirstName());
                System.out.println("Last Name: " + teacher.getLastName());
                System.out.println("Gender: " + teacher.getGender());
                System.out.println("Specialist: " + teacher.getSpecialist());
                System.out.println("Salary: " + teacher.getSalary());
                System.out.println("Status: " + teacher.getStatus());
                System.out.println("Created Date: " + teacher.getCreatedDate());
            }
        } else {
            System.out.println("Teacher is empty....");
        }
    }

    public void getDetail() {
        System.out.println("=== TEACHER DETAIL====");
        teacherController = new TeacherController();
        scanner = new Scanner(System.in);
        System.out.print("Enter id: ");
        int id = scanner.nextInt();
        Teacher teacher = teacherController.getDetail(id);
        System.out.println("Id: " + teacher.getId());
        System.out.println("First name: " + teacher.getFirstName());
        System.out.println("Last Name: " + teacher.getLastName());
        System.out.println("Gender: " + teacher.getGender());
        System.out.println("Specialist: " + teacher.getSpecialist());
        System.out.println("Salary: " + teacher.getSalary());
        System.out.println("Status: " + teacher.getStatus());
        System.out.println("Created Date: " + teacher.getCreatedDate());

    }

    public void add() {
        System.out.println("===== Add Teacher =====");
        scanner = new Scanner(System.in);
        teacher = new Teacher();
        teacherController = new TeacherController();

        System.out.print("First name: ");
        teacher.setFirstName(scanner.nextLine());

        System.out.print("Last name: ");
        teacher.setLastName(scanner.nextLine());

        System.out.print("Gender: ");
        teacher.setGender(scanner.nextLine());

        System.out.print("Specialist: ");
        teacher.setSpecialist(scanner.nextLine());

        System.out.print("Salary: ");
        teacher.setSalary(scanner.nextFloat());

        boolean isAdded = teacherController.add(teacher);
        if (isAdded) {
            System.out.println("Added successfully....");
        } else {
            System.out.println("Add fail.....");
        }
    }

    public void update() {
        System.out.println("=== Update Teacher ====");
        scanner = new Scanner(System.in);
        System.out.print("Enter id: ");
        int id = scanner.nextInt();

        scanner.nextLine();

        // Get teacher detail to display their original name
        teacherController = new TeacherController();
        Teacher originalTeacher = teacherController.getDetail(id);

        if (originalTeacher.getId() != 0) {

            teacher = new Teacher();

            System.out.print("Enter first name (" + originalTeacher.getFirstName() + ") : ");
            teacher.setFirstName(scanner.nextLine());

            System.out.print("Enter last name (" + originalTeacher.getLastName() + ") : ");
            teacher.setLastName(scanner.nextLine());

            System.out.print("Enter gender (" + originalTeacher.getGender() + ") : ");
            teacher.setGender(scanner.nextLine());

            System.out.print("Enter specialist (" + originalTeacher.getSpecialist() + ") : ");
            teacher.setSpecialist(scanner.nextLine());

            System.out.print("Enter salary (" + originalTeacher.getSalary() + ") : ");
            teacher.setSalary(scanner.nextFloat());

            boolean isSucceed = teacherController.update(id, teacher);
            if (isSucceed) {
                System.out.println("Teacher updated successful....");
            } else {
                System.out.println("Teacher updated fail.....");
            }
        } else {
            System.out.println("There is no teacher with id " + id);
        }

    }

    public void delete () {
        System.out.println("====== Delete ====");
        scanner = new Scanner(System.in);
        System.out.print("Enter id: ");
        int id = scanner.nextInt();

        teacherController = new TeacherController();
        Teacher originalTeacher =  teacherController.getDetail(id);
        if (originalTeacher.getId() != 0) {
            scanner.nextLine();
            System.out.print("Are you sure to delete " + originalTeacher.getFirstName() +  " (y)?");
            Character confirm = scanner.next().toUpperCase().charAt(0);
            if (confirm == 'Y') {
                boolean isDeleteSucceed = teacherController.delete(id);
                if (isDeleteSucceed)
                    System.out.println("Deleted successfully...");
                else
                    System.out.println("Deleted fail....");
            } else {
                System.out.println("You haven't delete " + originalTeacher.getFirstName());
            }
        } else {
            System.out.println("There is no teacher with id " + id);
        }
    }


    public void menu() {
        scanner = new Scanner(System.in);
        System.out.println("======= Teacher ==========");
        System.out.println("1. Get All Teachers \t 2. Get Teacher Detail \t 3. Add Teacher " +
                "\n 4. Edit Teacher \t 5. Delete Teacher \t 6. Back");
        try {
            System.out.print("-> ");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    getAll();
                    break;
                case 2:
                    getDetail();
                    break;
                case 3:
                    add();
                    break;
                case 4:
                    update();
                    break;
                case 5:
                    delete();
                    break;
                case 6:
                    new Main().mainMenu();
            }
        } catch (InputMismatchException e) {
            scanner.next();
            menu();
        }
        menu();
    }
}
