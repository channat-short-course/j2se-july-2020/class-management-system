package com.channat.views;

import com.channat.Main;
import com.channat.controllers.CourseController;
import com.channat.controllers.StudentController;
import com.channat.controllers.TeacherController;
import com.channat.models.dto.Course;
import com.channat.models.dto.Student;
import com.channat.models.dto.Teacher;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CourseView {

    private CourseController courseController = new CourseController();
    private Scanner scanner;
    private Course course;

    public void add() {
        courseController = new CourseController();
        scanner = new Scanner(System.in);
        course = new Course();

        System.out.print("Enter course name: ");
        course.setCourseName(scanner.nextLine());

        ArrayList<Teacher> teachers = new TeacherController().getAll();
        for (Teacher teacher : teachers) {
            String teacherDetail = teacher.getId() + " : " + teacher.getFirstName() + "-" + teacher.getLastName();
            System.out.println(teacherDetail);
        }
        System.out.print("Enter teacher id: ");
        int id = scanner.nextInt();
        Teacher teacher = new Teacher();
        for (int i = 0; i<teachers.size(); i++) {
            if (id == teachers.get(i).getId()) {
                teacher = teachers.get(i);
            }
        }

        char addStudent = 'N';
        ArrayList<Student> allStudents = new StudentController().getAll();
        ArrayList<Student> studentsToBeAdded = new ArrayList<>();
        do {
            scanner.nextLine();
            System.out.print("Add student (Y/N)? : ");
            addStudent = scanner.next().toUpperCase().charAt(0);
            if (addStudent == 'Y') {
                for (Student student : allStudents) {
                    String studentInfo = student.getId() + " : " + student.getFirstName() + "-" + student.getLastName();
                    System.out.println(studentInfo);
                }
                System.out.print("Enter student id: ");
                int studentId = scanner.nextInt();
                for (int i = 0; i<allStudents.size(); i++) {
                    if (studentId == allStudents.get(i).getId()) {
                        studentsToBeAdded.add(allStudents.get(i));
                    }
                }
            } else if (addStudent == 'N') {
                break;
            }
        } while (addStudent != 'Y' || addStudent != 'N');


        course.setTeacher(teacher);
        course.setStudents(studentsToBeAdded);

        boolean isSucceed = courseController.add(course);
        if (isSucceed) {
            System.out.println("Inserted done...");
        } else {
            System.out.println("Inserted fail...");
        }
    }

    private void getAll() {
        System.out.println("======== GET ALL ==============");
        ArrayList<Course> courses = courseController.getAll();
        for (Course course : courses) {
            System.out.println("---------------------");
            System.out.println("Id:  " + course.getId());
            System.out.println("Course Name: " + course.getCourseName());
            System.out.println("Teacher Name: " + course.getTeacher().getFirstName() + "-" + course.getTeacher().getLastName());
        }
    }

    private void getDetail() {
        System.out.println(" ====== COURSE DETAIL =========");
        ArrayList<Course> courses = courseController.getAll();
        for (Course course : courses) {
            String courseInfo = course.getId() + " : " + course.getCourseName();
            System.out.println(courseInfo);
        }
        scanner = new Scanner(System.in);
        System.out.print("Enter course id: ");
        int courseId = scanner.nextInt();
        for (int i=0; i<courses.size(); i++) {
            if (courseId == courses.get(i).getId()) {
                course = courses.get(i);
            }
        }
        if (course != null) {

            Course detailCourse = courseController.detail(courseId, course);

            String teacherInfo = detailCourse.getTeacher().getFirstName() + " " + detailCourse.getTeacher().getLastName();
            System.out.println("------------------------");
            System.out.println("Id: " + detailCourse.getId());
            System.out.println("Course name: " + detailCourse.getCourseName());
            System.out.println("Teacher name: " + teacherInfo);
            System.out.println("Number of student: " + detailCourse.getStudents().size());
            int i = 0;
            for (Student student : detailCourse.getStudents()) {
                i ++;
                String studentInfo = student.getFirstName() + " " + student.getLastName();
                System.out.println(i + " " +studentInfo);
            }
            System.out.println("-----------------");
        } else {
            System.out.println("There is no couse...");
        }
    }

    private void update() {
        System.out.println("===== UPDATE =======");
        ArrayList<Course> courses = courseController.getAll();
        for (Course course : courses) {
            String courseInfo = course.getId() + " : " + course.getCourseName();
            System.out.println(courseInfo);
        }
        scanner = new Scanner(System.in);
        System.out.print("Enter course id: ");
        int courseId = scanner.nextInt();
        Course originalCourse = new Course();

        for (int i=0; i<courses.size(); i++) {
            if (courseId == courses.get(i).getId()) {
                originalCourse = courses.get(i);
            }
        }
        scanner.nextLine();

        System.out.print("Name (" + originalCourse.getCourseName() +") : ");
        originalCourse.setCourseName(scanner.nextLine());
//        scanner.next();

        System.out.println("Teacher Info");
        ArrayList<Teacher> teachers = new TeacherController().getAll();
        for (Teacher teacher : teachers) {
            String teacherInfo = teacher.getId() + " : " + teacher.getFirstName() + " " + teacher.getLastName();
            System.out.println(teacherInfo);
        }

        String previousTeacher =  originalCourse.getTeacher().getFirstName() + " " + originalCourse.getTeacher().getLastName();
        System.out.print("Enter teacher Id ("+ previousTeacher +") :");
        int teacherId = scanner.nextInt();
        Teacher teacher = new Teacher();
        teacher.setId(teacherId);

        originalCourse.setTeacher(teacher);

        Boolean isSucceed = courseController.edit(courseId, originalCourse);

        if (isSucceed) {
            System.out.println("Updated successfully...");
        } else {
            System.out.println("Udated fail....");
        }


    }

    private void delete() {
        System.out.println("====== DELETE ======");
        ArrayList<Course> courses = courseController.getAll();
        for (Course course : courses) {
            String courseInfo = course.getId() + " : " + course.getCourseName();
            System.out.println(courseInfo);
        }
        scanner = new Scanner(System.in);
        System.out.print("Enter course id: ");
        int courseId = scanner.nextInt();
        char confirm = 'N';
        scanner.nextLine();
        do {
            System.out.print("Are you sure to delete (Y/N)? : ");
            confirm = scanner.next().toUpperCase().charAt(0);
            if (confirm == 'Y') {
                boolean isDeleted = courseController.delete(courseId);
                if (isDeleted) {
                    System.out.println("Deleted successfully...");
                } else {
                    System.out.println("Deleted fail....");
                }
                break;
            } else if (confirm == 'N'){
                break;
            }
        } while (confirm != 'N' || confirm != 'Y');

    }


    public void menu() {
        scanner = new Scanner(System.in);
        System.out.println("======= COURSE ==========");
        System.out.println("1. Get All Courses \t 2. Get Course Detail \t 3. Add Course " +
                "\n 4. Edit Course \t 5. Delete Course \t 6. Back");
        try {
            System.out.print("-> ");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    getAll();
                    break;
                case 2:
                    getDetail();
                    break;
                case 3:
                    add();
                    break;
                case 4:
                    update();
                    break;
                case 5:
                    delete();
                    break;
                case 6:
                    new Main().mainMenu();
            }
        } catch (InputMismatchException e) {
            scanner.next();
            menu();
        }
        menu();
    }


}
