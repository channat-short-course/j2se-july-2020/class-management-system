package com.channat.views;

/*
    1. Add Student
    2. Edit Student
    3. Delete Student (Change status to false)
    4. Retrieve Detail
    5. Retrieve List
  */

import com.channat.Main;
import com.channat.controllers.StudentController;
import com.channat.models.dto.Student;
import jdk.internal.util.xml.impl.Input;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class StudentView {

    Scanner scanner;
    Student student = new Student();
    StudentController studentController = new StudentController();

    public void add() {
        System.out.println("==== Add Student ====");
        scanner = new Scanner(System.in);

        System.out.print("Enter first name: ");
        student.setFirstName(scanner.nextLine());

        System.out.print("Enter last name: ");
        String lastName = scanner.nextLine();
        student.setLastName(lastName);

        System.out.print("Enter gender: ");
        student.setGender(scanner.next());

        boolean isInserted = studentController.add(student);
        if (isInserted) {
            System.out.println("Inserted....");
        } else {
            System.out.println("Insert fail....");
        }

    }

    public void getDetail() {
        scanner = new Scanner(System.in);
        System.out.print("Enter id: " );
        int id = scanner.nextInt();
        student = studentController.getDetail(id);
        if (student.getId() == 0 ) {
            System.out.println("There is no student with id: " + id);
        } else {
            System.out.println("=== Student Detail ====");
            System.out.println("Id: " + student.getId());
            System.out.println("First Name: " + student.getFirstName());
            System.out.println("Last Name: " + student.getLastName());
            System.out.println("Gender: " + student.getGender());
            System.out.println("Status: " + student.getStatus());
            System.out.println("Created Date " + student.getCreatedDate());
        }
    }

    public void update() {
        System.out.println("===== UPDATE ======");
        scanner = new Scanner(System.in);
        try {
            System.out.print("Enter id: " );
            int id = scanner.nextInt();
            student.setId(id);
            scanner.nextLine();

            System.out.print("Enter first name: ");
            student.setFirstName(scanner.nextLine());

            System.out.print("Enter last name: ");
            String lastName = scanner.nextLine();
            student.setLastName(lastName);

            System.out.print("Enter gender: ");
            student.setGender(scanner.next());

            boolean isUpdated = studentController.update(student);
            if (isUpdated) {
                System.out.println("Updated successfully...");
            } else {
                System.out.println("Updated fail.....");
            }
        } catch (InputMismatchException e) {
            update();
        }

        menu();

    }

    public void getAll() {
        ArrayList<Student> students = studentController.getAll();
        for (Student student : students) {
            System.out.println("------------");
            System.out.println("Id: " + student.getId());
            System.out.println("First Name: " + student.getFirstName());
            System.out.println("Last Name: " + student.getLastName());
            System.out.println("Gender: " + student.getGender());
            System.out.println("Status: " + student.getStatus());
            System.out.println("Created Date " + student.getCreatedDate());
        }

    }

    public void delete() {
        System.out.println("========= DELETE ==============");
        scanner = new Scanner(System.in);
        try {
            System.out.print("Enter id: ");
            int id = scanner.nextInt();

            // check student detail before delete
            student = studentController.getDetail(id);
            String fullName = student.getFirstName() + "-" + student.getLastName();
            scanner.nextLine();
            System.out.println("Are you sure to delete " + fullName.toUpperCase() + "?");
            System.out.print("Press y or yes to confirm: ");
            String confirm = scanner.nextLine();
            if (confirm.equalsIgnoreCase("y") || confirm.equalsIgnoreCase("yes")) {
                boolean isDeleted = studentController.delete(id);
                if (isDeleted) {
                    System.out.println("Deleted successfully...");
                } else {
                    System.out.println("Delete fail...");
                }
            } else {
                System.out.println("You haven't delete " + fullName.toUpperCase());
            }

        } catch (InputMismatchException e) {
            delete();
        }
        menu();
    }

    public void menu() {
        scanner = new Scanner(System.in);
        System.out.println("======= STUDENT ==========");
        System.out.println("1. Get All Student \t 2. Get Student Detail \t 3. Add Student " +
                "\n 4. Edit Student \t 5. Delete Student \t 6. Back");
        try {
            System.out.print("-> ");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    getAll();
                    break;
                case 2:
                    getDetail();
                    break;
                case 3:
                    add();
                    break;
                case 4:
                    update();
                    break;
                case 5:
                    delete();
                    break;
                case 6:
                    new Main().mainMenu();
            }
        } catch (InputMismatchException e) {
            scanner.next();
            menu();
        }
        menu();
    }

}
